import React from 'react';
import logo from './logo.svg';
import './App.css';

import FirstApp from './components/First.js';

function App() {
  return (
    <div className="App">
      <FirstApp/>
    </div>
  );
}

export default App;
