import React from 'react';

class FirstApp extends React.Component {

    state = {
        id: 2
    }

    render() {
        return (
            <React.Fragment>
                {this.state.id == 2 &&
                    <div>hello</div>
                }
                {this.state.id == 3 ?
                <div>hi</div>  
                    :
                    <React.Fragment>
                        {this.state.id == 4 ?
                        <React.Fragment>welcome</React.Fragment>
                        :
                        <React.Fragment>Bye</React.Fragment>
                        }
                    </React.Fragment>
                }
            </React.Fragment>

        );
    }
}

export default FirstApp;